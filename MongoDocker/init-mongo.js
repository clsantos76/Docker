// db.getSiblingDB () is equivalent to use admin;
db.getSiblingDB('admin')
    .createUser({
        user: 'user',
        pwd: 'user',
        roles: ['readWrite']
});